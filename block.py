class Block:

    def __init__(this, name):

        this.name = name
        this.outLinks = []
        this.rank = 1
        this.sum = 0

    # def addLinks(this, links):
    #
    #     this.outLinks = links


    def addLinks(this, links):

        for item in links:
            this.outLinks.append(item)


    def update(this, d_):

        this.rank = this.sum + d_
        this.sum = 0
