from block import Block

def normalize(blocks, d, iterations):

    for i in range(0, iterations):

        for j in range(len(blocks)):

            for k in range(len(blocks[j].outLinks)):

                sum = blocks[j].rank * d / len(blocks[j].outLinks)

                blocks[j].outLinks[k].sum += sum

        for j in range(len(blocks)):

            blocks[j].update((1-d))
#
# a = Block('a')
# b = Block('b')
# c = Block('c')
# d = Block('d')
#
# a.addLinks([b, c])
# b.addLinks([c])
# c.addLinks([a])
# d.addLinks([c])
